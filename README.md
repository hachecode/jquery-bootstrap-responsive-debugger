# Jquery Bootstrap Responsive Debugger

Just a jquery plugin to debug the Twitter Bootstrap responsive breakpoints.

![example.jpg](https://bitbucket.org/repo/dng6Md/images/3891607303-example.jpg)

### Usage

```
<!-- Load jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Load Twitter Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Load Jquery Bootstrap Responsive Debugger -->
<script src="../lib/jquery.bootstrap.responsive.debugger.min.js"></script>

<!-- Use plugin -->
<script>
$(document).ready(function() {
    $.BootstrapResponsiveDebugger();
});
</script>
```

### Configuration

You can configure the plugin to avoid debugging in production environment.

```js
$.BootstrapResponsiveDebugger({
	environment: 'production'
});
```

You can overwrite the default **#twbsrsdb** wrapper id
```js
$.BootstrapResponsiveDebugger({
	main_id: 'my_custom_id'
});
```

#### Get in touch

This is [my site](http://www.hachecode.com), and this is [me on twitter](http://twitter.com/hachecode).