$.BootstrapResponsiveDebugger = function (options) {
	var settings = $.extend({
		environment: "development",
		main_id: "twbsrsdb",
	}, options);

	if(settings.environment != "production"){
		var html =  '<div id="'+settings.main_id+'">';
			html += '<h3 class="v visible-lg"><span class="label label-success">LG MODE</span></h3>';
			html += '<h4 class="v visible-md"><span class="label label-info">MD MODE</span></h4>';
			html += '<h5 class="v visible-sm"><span class="label label-warning">SM MODE</span></h5>';
			html += '<h6 class="v visible-xs"><span class="label label-danger">XS MODE</span></h6>';
			html += '</div>';

			$('body').append(html);

			$('#'+settings.main_id).css('z-index', '9999');
			$('#'+settings.main_id).css('position', 'fixed');
			$('#'+settings.main_id).css('top', '0');
			$('#'+settings.main_id).css('right', '0');
			$('#'+settings.main_id).css('padding', '10px');
			$('#'+settings.main_id+' .v').css('margin', '0');
	}
};